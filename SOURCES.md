If you use one of these, **please donate to the author**. Donations allow artists to make more cool FOSS stuff for the community.

## Pablo Stanley

**Donate at:** https://pablostanley.gumroad.com/

**Projects:**
* [Humaaans](https://www.humaaans.com/)
* [Open Peeps](https://www.openpeeps.com/)
* [Open Doodles](https://www.opendoodles.com/)
* Miroodles

**License:** [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

## Lukasz Adam

**Donate at:** https://lukaszadam.gumroad.com/l/pBPJg

**Project:** ["Free Illustrations"](https://lukaszadam.com/illustrations)

**License:** [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

## Vijay Verma

**Donate at:** https://www.buymeacoffee.com/realvjy

**Project:** [illlustrations.co](https://illlustrations.co/)

**License:** [MIT](https://illlustrations.co/license/)

## Leni Kauffman

**Author site:** https://www.lenikauffman.com/ 

**Project:** [Fresh Folk](https://fresh-folk.com/)

**License:** [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
